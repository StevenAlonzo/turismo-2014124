/**
 * Created by Steven on 07/05/2016.
 */
module.exports = function(app){
    return{
        add:function (req,res) {
            var Historial = app.get('historial');
            Historial.create({
                complemento: req.body.complemento,


            }).then(function (historial){
                res.json(historial);
            })
        },
        list:function (req , res){
            var Historial  = app.get('historial');
            Historial.findAll().then(function (historiales){
                res.json(historiales);
            });
        },
        edit:function (req,res){
            var Historial  = app.get('historial');
            Historial.find(req.body.id_Rol).then(function (historial) {
                if(historial){
                    historial.updateAttributes({
                        complemento: req.body.complemento,

                    }).then(function (historial) {
                        res.json(historial);
                    });
                }else{
                    res.status(404).({message: "historial no encontrado"});
                }
            });
        },
        delete:function(req,res){
            var Historial  = app.get('rol');
            Historial.destroy({
                where:{
                    id_Historial: req.body.id_Historial
                }
            }).then(function (historial) {
                res.json(historial );
            });

        },
        porid:function (req,res){
            var Historial  = app.get('historial');
            Historial.find(req.body.id_Historial).then(function (historial){
                if (historial){
                    res.json(historial);
                }else{
                    res.status(404).({message: "historial no encontrado"});
                }
            });
        },
        historialconusuario:function (req,res){
            var Historial = app.get('historial');
            var Usuario = app.get('usuario');
            Historial.find({where:{id_Historial: req.params.id},include: (Usuario)}).then(function (Historial) {
                res.json(Historial);
            })
        },

        historialconcentro:function (req,res){
            var Historial = app.get('historial');
            var CentroTuristico = app.get('centroturistico');
            Historial.find({where:{id_Historial: req.params.id},include: (CentroTuristico)}).then(function (Historial) {
                res.json(Historial);
            })
        },
    }
}