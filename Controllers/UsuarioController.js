/**
 * Created by Steven on 06/05/2016.
 */
module.exports = function(app){
    return{
        add:function (req,res) {
            var Usuario = app.get('usuario');
            Usuario.create({
                nombre: req.body.nombre,
                nacionalidad: req.body.nacionalidad,
                celular: req.body.celular,
                nick: req.body.nick,
                password: req.body.password,

            }).then(function (usuario){
                res.json(usuario);
            })
        },
        list:function (req , res){
            var Usuario  = app.get('usuario');
            Usuario.findAll().then(function (usuarios){
               res.json(usuarios);
            });
        },
        edit:function (req,res){
            var Usuario  = app.get('usuario');
            Usuario.find(req.body.id_Usuario).then(function (usuario) {
                if(usuario){
                    usuario.updateAttributes({
                        nombre: req.body.nombre,
                        nacionalidad: req.body.nacionalidad,
                        celular: req.body.celular,
                        nick: req.body.nick,
                        password: req.body.password,
                    }).then(function (usuario) {
                        res.json(usuario);
                    });
                }else{
                    res.status(404).send
                    
                    
                    
                    ({message: "Usuario no encontrado"});
                }
            });
        },
        delete:function(req,res){
            var Usuario  = app.get('usuario');
            usuario.destroy({
                where:{
                    id_Usuario: req.body.id_Usuario
                }
            }).then(function (usuario) {
                  res.json(usuario );
            });

        },
        porid:function (req,res){
            var Usuario  = app.get('usuario');
            Usuario.find(req.body.id_Usuario).then(function (usuario){
                if (usuario){
                    res.json(usuario);
                }else{
                    res.status(404).send({message: "Usuario no encontrado"});
                }
            });
        },
        usuarioconrol:function (req,res){
            var Usuario = app.get('usuario');
            var Rol = app.get('rol');
            Usuario.find({where:{id_Usuario: req.params.id},include: (Rol)}).then(function (Usuario) {
                res.json(Usuario);
            })

    }
}}