/**
 * Created by Steven on 06/05/2016.
 */
module.exports = function(app){
    return{
        add:function (req,res) {
            var CentroTuristico = app.get('centroturistico');
            CentroTuristico.create({
                nombre: req.body.nombre,
                latitud: req.body.latitud,
                longitud: req.body.longitud,
                informacion: req.body.informacion,
                tipoVisitantes: req.body.tipoVisitantes,
                actividades: req.body.actividades,
                precio: req.body.precio,
                departamento:req.body.departamento,

            }).then(function (centroturistico){
                res.json(centroturistico);
            })
        },
        list:function (req , res){
            var CentroTuristico  = app.get('centroturistico');
            CentroTuristico.findAll().then(function (centroturisticos){
                res.json(centroturisticos);
            });
        },
        edit:function (req,res){
            var CentroTuristico  = app.get('centroturistico');
            CentroTuristico.find(req.body.id_CentroTuristico).then(function (centroturistico) {
                if(centroturistico){
                    centroturistico.updateAttributes({
                        nombre: req.body.nombre,
                        latitud: req.body.latitud,
                        longitud: req.body.longitud,
                        informacion: req.body.informacion,
                        tipoVisitantes: req.body.tipoVisitantes,
                        actividades: req.body.actividades,
                        precio: req.body.precio,
                        departamento:req.body.departamento,
                    }).then(function (centroturistico) {
                        res.json(centroturistico);
                    });
                }else{
                    res.status(404).send({message: "centroturistico no encontrado"});

                }
            });
        },
        delete:function(req,res){
            var CentroTuristico  = app.get('centroturistico');
            centroturistico.destroy({
                where:{
                    id_CentroTuristico: req.body.id_CentroTuristico
                }
            }).then(function (centroturistico) {
                res.json(centroturistico );
            });

        },
        porid:function (req,res){
            var CentroTuristico  = app.get('centroturistico');
            CentroTuristico.find(req.body.id_CentroTuristico).then(function (centroturistico){
                if (centroturistico){
                    res.json(centroturistico);
                }else{
                    res.status(404).send({message: "centroturistico no encontrado"});
                }
            });
        },
        centroconfoto:function (req,res){
            var CentroTuristico = app.get('centroturistico');
            var Foto = app.get('foto');
            CentroTuristico.find({where:{id_CentroTuristico: req.params.id},include: (Foto)}).then(function (CentroTuristico) {
                res.json(CentroTuristico);
            })

        }
    }}