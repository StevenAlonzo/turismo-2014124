/**
 * Created by Steven on 07/05/2016.
 */
module.exports = function(app){
    return{
        add:function (req,res) {
            var Foto = app.get('foto');
            Foto.create({
                imagen: req.body.imagen,


            }).then(function (foto){
                res.json(foto);
            })
        },
        list:function (req , res){
            var Foto  = app.get('foto');
            Foto.findAll().then(function (fotos){
                res.json(fotos);
            });
        },
        edit:function (req,res){
            var Foto  = app.get('foto');
            Foto.find(req.body.id_Foto).then(function (foto) {
                if(foto){
                    foto.updateAttributes({
                        imagen: req.body.imagen,

                    }).then(function (foto) {
                        res.json(foto);
                    });
                }else{
                    res.status(404).({message: "foto no encontrado"});
                }
            });
        },
        delete:function(req,res){
            var Foto  = app.get('foto');
            foto.destroy({
                where:{
                    id_Foto: req.body.id_Foto
                }
            }).then(function (foto) {
                res.json(foto );
            });

        },
        porid:function (req,res){
            var Foto  = app.get('foto');
            Foto.find(req.body.id_Foto).then(function (foto){
                if (foto){
                    res.json(foto);
                }else{
                    res.status(404).({message: "foto no encontrado"});
                }
            });
        }
    }
}