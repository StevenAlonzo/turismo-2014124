/**
 * Created by Steven on 06/05/2016.
 */
module.exports = function(app){
    return{
        add:function (req,res) {
            var Comentario = app.get('comentario');
            Comentario.create({
                texto: req.body.texto,


            }).then(function (comentario){
                res.json(comentario);
            })
        },
        list:function (req , res){
            var Comentario  = app.get('comentario');
            Comentario.findAll().then(function (comentarios){
                res.json(comentarios);
            });
        },
        edit:function (req,res){
            var Comentario  = app.get('comentario');
            Comentario.find(req.body.id_Comentario).then(function (comentario) {
                if(comentario){
                    comentario.updateAttributes({
                        texto: req.body.texto,

                    }).then(function (comentario) {
                        res.json(comentario);
                    });
                }else{
                    res.status(404).({message: "comentario no encontrado"});
                }
            });
        },
        delete:function(req,res){
            var Comentario  = app.get('comentario');
            comentario.destroy({
                where:{
                    id_Comentario: req.body.id_Comentario
                }
            }).then(function (comentario) {
                res.json(comentario );
            });

        },
        porid:function (req,res){
            var Comentario  = app.get('comentario');
            Comentario.find(req.body.id_Comentario).then(function (comentario){
                if (comentario){
                    res.json(comentario);
                }else{
                    res.status(404).({message: "comentario no encontrado"});
                }
            });
        },
        comentarioconusuario:function (req,res){
            var Comentario = app.get('comentario');
            var Usuario = app.get('usuario');
            Comentario.find({where:{id_Comentario: req.params.id},include: (Usuario)}).then(function (Comentario) {
                res.json(Comentario);
            })
        },

        comentarioconcentro:function (req,res){
            var Comentario = app.get('comentario');
            var CentroTuristico = app.get('centroturistico');
            Comentario.find({where:{id_Comentario: req.params.id},include: (CentroTuristico)}).then(function (Comentario) {
                res.json(Comentario);
            })
        },
    }
}
