/**
 * Created by Steven on 06/05/2016.
 */
module.exports = function(app){
    return{
        add:function (req,res) {
            var Rol = app.get('rol');
            Rol.create({
                nombre: req.body.nombre,
               

            }).then(function (rol){
                res.json(rol);
            })
        },
        list:function (req , res){
            var Rol  = app.get('rol');
            Rol.findAll().then(function (roles){
                res.json(roles);
            });
        },
        edit:function (req,res){
            var Rol  = app.get('rol');
            Rol.find(req.body.id_Rol).then(function (rol) {
                if(rol){
                    rol.updateAttributes({
                        nombre: req.body.nombre,
                       
                    }).then(function (rol) {
                        res.json(rol);
                    });
                }else{
                    res.status(404).({message: "Rol no encontrado"});
                }
            });
        },
        delete:function(req,res){
            var Rol  = app.get('rol');
            rol.destroy({
                where:{
                    id_Rol: req.body.id_Rol
                }
            }).then(function (rol) {
                res.json(rol );
            });

        },
        porid:function (req,res){
            var Rol  = app.get('rol');
            Rol.find(req.body.id_Usuario).then(function (rol){
                if (rol){
                    res.json(rol);
                }else{
                    res.status(404).({message: "Rol no encontrado"});
                }
            });
        }
    }
}