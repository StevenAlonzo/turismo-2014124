/**
 * Created by Steven on 06/05/2016.
 */
var ruta = require('express').Router();
module.exports =(function(app){
        var usuario = require('../Controllers/UsuarioController')(app);
        var rol = require('../Controllers/RolController')(app);
        var centroturistico =  require('../Controllers/CentroTuristicoController')(app);
        var foto = require('../Controllers/FotoController')(app);
        var comentario =  require('../Controllers/ComentarioController')(app);


    //rutas Usuarios
    ruta.get('/usuario', usuario.list);
    ruta.post('/usuario',usuario.add);
    ruta.put('/usuario',usuario.edit);
    ruta.delete('/usuario', usuario.delete);
    ruta.get('/usuario/:id', usuario. usuarioconrol);

    //rutas Centro
    ruta.get('/centroturistico', centroturistico.list);
    ruta.post('/centroturistico',centroturistico.add);
    ruta.put('/centroturistico',centroturistico.edit);
    ruta.delete('/centroturistico', centroturistico.delete);
    ruta.get('/centroturistico/:id', centroturistico. centroconfoto);

    //rutas Rol
    ruta.get('/rol', rol.list);
    ruta.post('/rol',rol.add);
    ruta.put('/rol',rol.edit);
    ruta.delete('/rol', rol.delete);

    //rutas comentario
    ruta.get('/comentario', comentario.list);
    ruta.post('/comentario',comentario.add);
    ruta.put('/comentario',comentario.edit);
    ruta.delete('/comentario', comentario.delete);
    ruta.get('/comentario/:id', comentario. comentarioconusuario());
    ruta.get('/comentario/:id', comentario. comentarioconcentro());

    //rutas Foto
    ruta.get('/foto', foto.list);
    ruta.post('/foto',foto.add);
    ruta.put('/foto',foto.edit);
    ruta.delete('/foto', foto.delete);

    //rutas Historial
    ruta.get('/historial', historial.list);
    ruta.post('/historial',historial.add);
    ruta.put('/historial',historial.edit);
    ruta.delete('/historial', historial.delete);
    ruta.get('/historial/:id', historial. historialconusuario());
    ruta.get('/historial/:id', historial. historialconcentro());

    return ruta;
})