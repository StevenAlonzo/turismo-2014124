/**
 * Created by informatica on 06/05/2016.
 */
(function() {
    var express = require('express');
    var bodyParser = require('body-parser');
    var morgan=require('morgan');
    var mysql = require('mysql');
    var Sequelize = require('sequelize');

    var sequelize = new Sequelize('db_turismo','root','',{
        host: 'localhost',
        dialect: 'mysql',
        pool: {
            max: 20,
            min: 0,
            idle: 10000
        }
    });

    /*
        Declaracion Modelos por ORM
     */
    var Rol = sequelize.define('rol',{
        id_Rol: {type: Sequelize.INTEGER, primaryKey: true, autoIncrement: true  },
        nombre: {type: Sequelize.STRING, allowNull: false},
    });

    var Usuario = sequelize.define('usuario',{
        id_Usuario: {type: Sequelize.INTEGER, primaryKey: true, autoIncrement: true  },
        nombre: {type: Sequelize.STRING, allowNull: false},
        nacionalidad: {type: Sequelize.STRING, allowNull: false},
        celular: {type: Sequelize.INTEGER, allowNull: false},
        nick: {type: Sequelize.STRING, allowNull: false},
        password: {type: Sequelize.STRING, allowNull: false},
        id_Rol: {type: Sequelize.INTEGER , foreignKey:true}

        

    });

    var CentroTuristico = sequelize.define('centroturistico',{
        id_CentroTuristico: {type: Sequelize.INTEGER, primaryKey: true, autoIncrement: true  },
        nombre: {type: Sequelize.STRING, allowNull: false},
        latitud: {type: Sequelize.DOUBLE, allowNull: false},
        longitud: {type: Sequelize.DOUBLE, allowNull: false},
        informacion: {type: Sequelize.STRING, allowNull: false},
        tipoVisitantes: {type: Sequelize.STRING, allowNull: false},
        actividades: {type: Sequelize.STRING, allowNull: false},
        precio: {type: Sequelize.STRING, allowNull: false},
        departamento: {type: Sequelize.STRING, allowNull: false},
        id_Foto: {type: Sequelize.INTEGER , foreignKey:true},
    });

    var Historial = sequelize.define('historial',{
        complemento: {type: Sequelize.STRING, allowNull: false},
        id_CentroTuristico: {type: Sequelize.INTEGER , foreignKey:true},
            
        id_Usuario: {type: Sequelize.INTEGER , foreignKey:true}

    });

    var Comentario = sequelize.define('comentario' ,{
        id_Comentario: {type: Sequelize.INTEGER , primaryKey: true, autoIncrement: true},
        texto: {type: Sequelize.STRING, null:false},
        id_CentroTuristico: {type: Sequelize.INTEGER , foreignKey:true},

        id_Usuario: {type: Sequelize.INTEGER , foreignKey:true}

    });

    var Foto = sequelize.define('foto',{
        id_Foto: {type: Sequelize.INTEGER, primaryKey:true, autoIncrement:true},
        imagen: {type: Sequelize.Image,primaryKey:true, autoIncrement:true},
    });

    //usuario
    Rol.hasMany(Usuario, {foreignKey:'id_Rol', constraints:true});
    Usuario.belongsTo(Rol, {foreignKey:'id_Rol', constraints:true});
    //historial
    CentroTuristico.hasMany(Historial, {foreignKey:'id_CentroTuristico', constraints:true});
    Historial.belongsTo(CentroTuristico, {foreignKey:'id_CentroTuristico', constraints:true});
    Usuario.hasMany(Historial, {foreignKey:'id_Usuario', constraints:true});
    Historial.belongsTo(Usuario, {foreignKey:'id_Usuario', constraints:true});
    //comentario
    CentroTuristico.hasMany(Comentario, {foreignKey:'id_CentroTuristico', constraints:true});
    Comentario.belongsTo(CentroTuristico, {foreignKey:'id_CentroTuristico', constraints:true});
    Usuario.hasMany(Comentario, {foreignKey:'id_Usuario', constraints:true});
    Comentario.belongsTo(Usuario, {foreignKey:'id_Usuario', constraints:true});
    //Foto
    Foto.hasMany(CentroTuristico,{foreignKey:'id_Foto' , constraints:true});
    CentroTuristico.belongsTo(Foto, {foreignKey:'id_Foto', constraints:true});

    sequelize.sync({force:true});
    var puerto=3000
    var conf=require('./config');
    var app=express();
    app.use(bodyParser.urlencoded({
        extended:true
    }));
    app.use(bodyParser.json());
    app.use('/api/v1',require('.routes/' )(app));
    app.use(morgan('dev'));
    app.set('usuario',Usuario);
    app.set('centroturistico',CentroTuristico);
    app.set('historial',Historial);
    app.set('foto',Foto);
    app.set('comentario',Comentario);
    app.listen(puerto,function () {
        console.log("servidor iniciado en el puerto: " + puerto);
        console.log("Debug server:");
    });

})();